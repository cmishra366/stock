#from django.contrib.auth.models import User
from rest_framework import serializers
from calc.models import registrationData


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = registrationData
        fields =['firstname', 'lastname','username','password']

    def create(self, validated_data):
        user=registrationData().objects.create(
            firstname=self.validated_data['firstname'],
            lastname=self.validated_data['lastname'],
            username=self.validated_data['username'],
            password=self.validated_data['password']
        )
        user.save()
        return user